import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(home: HomePage(),debugShowCheckedModeBanner: false));


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text("Quran Dialog"),
          onPressed: () => _showDialog(context),
        ),
      ),
    );
  }
}

Future _showDialog(BuildContext context) async {
  await showDialog(
    context: context,
    builder: (BuildContext context) {
      return QuranDialog();
    }
  );
}


class QuranDialog extends StatefulWidget {
  @override
  _QuranDialogState createState() => _QuranDialogState();
}

class _QuranDialogState extends State<QuranDialog> {

  List <DropdownMenuItem<String>> _dropDownMenuItems;
  String _sel;

  List <DropdownMenuItem<String>> _dropDownMenuItems2;
  String _sel2;

 @override
  void initState() {
    super.initState();

    _dropDownMenuItems = _getDropDownMenuItems();
    _sel = _dropDownMenuItems[0].value;

    _dropDownMenuItems2 = _getDropDownMenuItems2();
    _sel2 = _dropDownMenuItems2[0].value;
 }

 List <DropdownMenuItem<String>> _getDropDownMenuItems() {
   List <DropdownMenuItem<String>> items = List();
   
   items.add(DropdownMenuItem(
     value: 'الفاتحة 1',
     child: Text("الفاتحة 1"),
   ));

   items.add(DropdownMenuItem(
     value: 'الفاتحة 7',
     child: Text("الفاتحة 7"),
   ));
   return items;
 }

  List <DropdownMenuItem<String>> _getDropDownMenuItems2() {
    List <DropdownMenuItem<String>> items = List();

    items.add(DropdownMenuItem(
      value: 'الفاتحة 7',
      child: Text("الفاتحة 7"),
    ));

    items.add(DropdownMenuItem(
      value: 'الفاتحة 1',
      child: Text("الفاتحة 1"),
    ));
    return items;
  }

 void changedDropDownItem(String selectedItem) {
   setState(() {
     _sel = selectedItem;
   });
 }

  void changedDropDownItem2(String selectedItem) {
    setState(() {
      _sel2 = selectedItem;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: 305,
        alignment: Alignment.centerRight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[

            //تكرار الآيات
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                      maxRadius: 14,
                      backgroundColor: Colors.green.withGreen(200),
                      child: Icon(Icons.keyboard_arrow_up,color: Colors.white,size: 27,)),

                    SizedBox(width: 7),

                    Text("۰"),

                    SizedBox(width: 7),

                    CircleAvatar(
                      maxRadius: 14,
                        backgroundColor: Colors.green.withGreen(200),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 1.5,left: 1),
                        child: Icon(Icons.keyboard_arrow_down,color: Colors.white,size: 27,),
                      )),
                  ],
                ),



                Text("تكرار الآيات",style: TextStyle(
                  color: Colors.teal[600],
                ),),
              ],
            ),

            SizedBox(height: 10),

            Row(
             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
             children: <Widget>[

               Container(
                 decoration: BoxDecoration(
                   border: Border.all(color: Colors.brown[200],width: 1.5),
                   borderRadius: BorderRadius.circular(7)
                 ),
                 padding: EdgeInsets.only(left: 5),
                 height: 35,
                 width: 150,
                 child: DropdownButtonHideUnderline(
                   child: DropdownButton(
                     value: _sel,
                     items: _dropDownMenuItems,
                     onChanged: changedDropDownItem,
                     isExpanded: true,
                   ),
                 ),
               ),

               Text("من",style: TextStyle(
                   color: Colors.teal[600]
               ),),
             ],
           ),

            SizedBox(height: 10),

            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[

                Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.brown[200],width: 1.5),
                      borderRadius: BorderRadius.circular(7)
                  ),
                  padding: EdgeInsets.only(left: 5),
                  height: 35,
                  width: 150,
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(
                      value: _sel2,
                      items: _dropDownMenuItems2,
                      onChanged: changedDropDownItem2,
                      isExpanded: true,
                    ),
                  ),
                ),

                Text("إلى",style: TextStyle(
                    color: Colors.teal[600]
                ),),
              ],
            ),

            Divider(color: Colors.black),

            //تكرار النطاق
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                        maxRadius: 14,
                        backgroundColor: Colors.green.withGreen(200),
                        child: Icon(Icons.keyboard_arrow_up,color: Colors.white,size: 27,)),

                    SizedBox(width: 7),

                    Text("۰"),

                    SizedBox(width: 7),

                    CircleAvatar(
                        maxRadius: 14,
                        backgroundColor: Colors.green.withGreen(200),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 1.5,left: 1),
                          child: Icon(Icons.keyboard_arrow_down,color: Colors.white,size: 27,),
                        )),
                  ],
                ),



                Text("تكرار النطاق",style: TextStyle(
                  color: Colors.teal[600],
                ),),
              ],
            ),

            Divider(color: Colors.black),

            Center(
              child: Text("تلاوة معلم",style: TextStyle(
                color: Colors.teal[600],
                fontWeight: FontWeight.bold
              ),),
            ),

            //زمن السكوت بين الآيات
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(
                        maxRadius: 14,
                        backgroundColor: Colors.green.withGreen(200),
                        child: Icon(Icons.keyboard_arrow_up,color: Colors.white,size: 27,)),

                    SizedBox(width: 7),

                    Text("۰"),

                    SizedBox(width: 7),

                    CircleAvatar(
                        maxRadius: 14,
                        backgroundColor: Colors.green.withGreen(200),
                        child: Padding(
                          padding: const EdgeInsets.only(top: 1.5,left: 1),
                          child: Icon(Icons.keyboard_arrow_down,color: Colors.white,size: 27,),
                        )),
                  ],
                ),



                Text("زمن السكوت بين الآيات",style: TextStyle(
                  color: Colors.teal[600],
                  fontSize: 14,
                ),),
              ],
            ),

            SizedBox(height: 5),

            Center(
              child: RaisedButton(
                child: Text("ابدأ التكرار",style: TextStyle(
                  color: Colors.white
                ),),
                color: Colors.teal[600],
                onPressed: () {},
              ),
            )
          ],
        ),
      )
    );
  }
}








